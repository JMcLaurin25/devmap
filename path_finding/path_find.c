
#include <inttypes.h>
#include <math.h>
#include <stdio.h>

#include "hash.h"
#include "heap.h"
#include "path_find.h"

/*
For a thorough documentation see /docs/index.html
*/

/*!\file
	Path_find functions were built off of foundational exercises
	in datastructure lessons. 
	https://github.com/umbctraining/datastructures/tree/master/graph/
 */

//!Returns the priority delta 
int pq_compare(void *a, void *b)
{
	if(!a) {
		return -(intptr_t)b;
	}
	if(!b) {
		return (intptr_t)a;
	}

	struct pqueue_node *pqa, *pqb;
	pqa = (struct pqueue_node *)a;
	pqb = (struct pqueue_node *)b;

	return pqa->priority - pqb->priority;
}

//!Creates a node for the priority queue
struct pqueue_node *__make_node(size_t dev_id, int priority)
{
	struct pqueue_node *pqn = malloc(sizeof(*pqn));
	if(!pqn) {
		return NULL;
	}
	pqn->dev_id = dev_id;
	pqn->priority = priority;

	return pqn;
}

//!Creates a visited node
struct visited_node *__make_vnode(int distance, struct pqueue_node *priority, void *prev)
{
	struct visited_node *vis = malloc(sizeof(*vis));
	if(!vis) {
		return NULL;
	}
	vis->prev = *(size_t *)prev;
	vis->distance = distance;
	vis->priority = priority;

	return vis;
}

/*!
Finds the shortest path between 2 connected nodes in a graph
*/
struct llist *dijkstra_path(graph *g, size_t src, size_t dst)
{
	//Initialize the heap, create a priority queue node and add it
	heap *to_process = heap_create(pq_compare);
	struct pqueue_node *start =__make_node(src, 0);
	heap_add(to_process, start);
	//Initialize the hash, create a visited node and add it
	hash *visited = hash_create();
	size_t null = 0;
	struct visited_node *first = __make_vnode(0, start, &null);

	hash_insert(visited, src, (void *)first);
	//Iterate the heap until destination device is found
	while(!heap_is_empty(to_process)) {
		struct pqueue_node *cur_pq = heap_remove_min(to_process);
		if(cur_pq->dev_id == dst) {
			goto FOUND;
		}

		struct llist *adjacencies = graph_adjacent_to(g, &(cur_pq->dev_id));
		struct llist *check = adjacencies;

		while (check) {
			int dist = cur_pq->priority + graph_edge_weight(g, (void *)cur_pq, check->dev_id);
			if(!hash_exists(visited, check->dev_id)) {
				struct pqueue_node *pq_to_add = __make_node(check->dev_id, dist);
				struct visited_node *next_node = __make_vnode(dist, pq_to_add, &(cur_pq->dev_id));
				if (hash_insert(visited, check->dev_id, (void *)next_node)) {
					heap_add(to_process, pq_to_add);
				} else {
					free(pq_to_add);
					free(next_node);
				}
			} else {
				struct visited_node *found = hash_fetch(visited, check->dev_id);
				//If shorter path found
				if(dist < found->distance) {
					found->distance = dist;
					found->prev = cur_pq->dev_id;
					found->priority->priority = dist;
					heap_rebalance(to_process);
				}
			}
			check = check->next;
		}
		ll_disassemble(adjacencies);
	}
	heap_disassemble(to_process);
	hash_destroy(visited);

	//No path found
	return NULL;

FOUND:
	heap_disassemble(to_process);

	//create the path to the destination
	struct llist *path = ll_create(dst);

	//add visited nodes to path
	//hash_fetch returns a void * to type visited_node
	while (((struct visited_node *)hash_fetch(visited, path->dev_id))->prev) {
		ll_add(&path, ((struct visited_node *)hash_fetch(visited, path->dev_id))->prev);
	}

	hash_destroy(visited);
	return path;
}

/*!
Returns the calculated distance of two devices utilizing Haversines formula
and the pythagoreum theorum.
 https://en.wikipedia.org/wiki/Haversine_formula

Earths mean radius = 6,381.1km (3,963.2mi)
 https://en.wikipedia.org/wiki/Earth
*/
double device_distance(struct device *dev1, struct device *dev2)
{
	if (!dev1 || !dev2) {
		return 0;
	}

	const double earth = 6378.1;
	double lat1, lat2, long1, long2;
	double hav = 0, rads = 0, gc_dist = 0, dlat = 0, dlong = 0, dev_dist = 0;
	float dalt = 0;

	//Convert degrees to radians
	lat1 = dev1->latitude * (M_PI/180);
	lat2 = dev2->latitude * (M_PI/180);
	long1 = dev1->longitude * (M_PI/180);
	long2 = dev2->longitude * (M_PI/180);

	//device deltas
	dlat = fabs(lat2 - lat1);
	dlong = fabs(long2 - long1);
	dalt = fabs(dev2->altitude - dev1->altitude);

	hav = pow(sin(dlat / 2), 2) + cos(lat1) * cos(lat2) * pow(sin(dlong / 2), 2);
	rads = 2 * atan2(sqrt(hav), sqrt(1 - hav));

	gc_dist = (earth * rads) * 1000;//Great-circle distance in meters

	dev_dist = sqrt(pow(gc_dist, 2) + pow(dalt, 2)); //Actual distance using pythagoreum theorum

	return dev_dist; 
}

//!Searches the Graph for devices without multiple connecting edges
struct llist *find_single_dev(graph *g)
{
	if (!g) {
		return NULL;
	}

	if (graph_node_count(g) < 3) {
		return NULL;
	}

	struct llist *removal_list = NULL;

	struct node *device = g->nodes;
	while (device) {

		struct node *next = device->next;
		while (next) {
			graph *copy_a = graph_copy(g);
			if (!copy_a) {
				return NULL;
			}

			size_t path_count = 0;
			struct llist *path = NULL;
			while ((path = dijkstra_path(copy_a, device->data->dev_id, next->data->dev_id)) != NULL) {
				path_remove(copy_a, path);

				path_count++;
				ll_disassemble(path);
			}
			if (path_count < 2) {
				if (removal_list == NULL) {
					removal_list = ll_create(device->data->dev_id);
					if (!removal_list) {
						return NULL;
					}
				} else {
					if (!ll_fetch(removal_list, device->data->dev_id)) {
						ll_add(&removal_list, device->data->dev_id);
					}
				}
			}

			next = next->next;
			graph_disassemble(copy_a);
		}

		device = device->next;
	}

	return removal_list;
}

//!Remove a node path from the graph
void path_remove(graph *g, struct llist *path)
{
	if (!g || !path) {
		return;
	}

	while (path->next) {
		size_t src = path->dev_id;
		size_t dst = path->next->dev_id;
		graph_remove_edge(g, src, dst);

		path = path->next;
	}
}

void print_item(void *data, bool is_node)
{
	if(is_node) {
		printf("\n%s", (char *)data);
	} else {
		printf(u8" → %s", (char *)data);
	}
}

//!Prints the path of node to node
void print_path(struct llist *path)
{
	printf("  ");
	while (path) {
		printf(u8"%10lu → ", path->dev_id);
		path = path->next;
	}
	printf("\n");
}
