#ifndef PATH_FIND_H
#define PATH_FIND_H

#include "graph.h"
#include "llist.h"
#include "queue.h"

struct pqueue_node {
	size_t dev_id;
	int priority;
};

struct visited_node {
	int distance;
	struct pqueue_node *priority;
	size_t prev;
	//void *prev;
};

int pq_compare(void *a, void *b);

struct pqueue_node *__make_node(size_t dev_id, int priority);
struct visited_node *__make_vnode(int distance, struct pqueue_node *priority, void *prev);

struct llist *dijkstra_path(graph *g, size_t src, size_t dst); 

double device_distance(struct device *dev1, struct device *dev2);

struct llist *find_single_dev(graph *g);
void path_remove(graph *g, struct llist *path);

void print_item(void *data, bool is_node);
void print_path(struct llist *path);

#endif
