var searchData=
[
  ['batt_5fll_5fdestroy',['batt_ll_destroy',['../llist_8h.html#a1b83709ecedfc2324289821a21a698ab',1,'llist.h']]],
  ['batt_5fll_5fprint',['batt_ll_print',['../llist_8c.html#a59cd075216a98aa85404750fd3045f05',1,'batt_ll_print(struct llist *a):&#160;llist.c'],['../llist_8h.html#a59cd075216a98aa85404750fd3045f05',1,'batt_ll_print(struct llist *a):&#160;llist.c']]],
  ['batt_5fpwr',['batt_pwr',['../structdevice.html#afecf532dd36ae1360e5eeab0724779cc',1,'device']]],
  ['battery_5farray',['battery_array',['../structstatus__payload.html#a93176abe9b2a5ebd124a005a1e2924cb',1,'status_payload']]],
  ['battery_5fprint',['battery_print',['../graph_8c.html#a9bd6db6de10ce5742fab79fee234b1ed',1,'battery_print(graph *g, size_t level):&#160;graph.c'],['../graph_8h.html#a9bd6db6de10ce5742fab79fee234b1ed',1,'battery_print(graph *g, size_t level):&#160;graph.c']]],
  ['battery_5fpwr',['battery_pwr',['../structstatus__payload.html#a67f3834e9172f9dfa4f0a6ebd495c89b',1,'status_payload']]],
  ['battery_5fupdate',['battery_update',['../graph_8c.html#a53106991309bb1613607296200f93e02',1,'battery_update(graph *g, size_t dev_id, size_t pwr_lvl):&#160;graph.c'],['../graph_8h.html#a53106991309bb1613607296200f93e02',1,'battery_update(graph *g, size_t dev_id, size_t pwr_lvl):&#160;graph.c']]]
];
