var indexSectionsWithContent =
{
  0: "_abcdefghilmnopqrstvw",
  1: "_cdeghlmnpqsv",
  2: "dghlpq",
  3: "_bcdfghilmpqs",
  4: "abcdeghilmnopstvw",
  5: "gh",
  6: "lpr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros"
};

