var annotated =
[
    [ "_adjllist_graph", "struct__adjllist__graph.html", "struct__adjllist__graph" ],
    [ "_hash", "struct__hash.html", "struct__hash" ],
    [ "_heap", "struct__heap.html", "struct__heap" ],
    [ "command_payload", "structcommand__payload.html", "structcommand__payload" ],
    [ "data_payload", "uniondata__payload.html", "uniondata__payload" ],
    [ "device", "structdevice.html", "structdevice" ],
    [ "edge", "structedge.html", "structedge" ],
    [ "gps_payload", "structgps__payload.html", "structgps__payload" ],
    [ "h_llist", "structh__llist.html", "structh__llist" ],
    [ "llist", "structllist.html", "structllist" ],
    [ "meditrik", "structmeditrik.html", "structmeditrik" ],
    [ "message_payload", "structmessage__payload.html", "structmessage__payload" ],
    [ "node", "structnode.html", "structnode" ],
    [ "pkt_format", "structpkt__format.html", "structpkt__format" ],
    [ "pqueue_node", "structpqueue__node.html", "structpqueue__node" ],
    [ "queue", "structqueue.html", "structqueue" ],
    [ "status_payload", "structstatus__payload.html", "structstatus__payload" ],
    [ "visited_node", "structvisited__node.html", "structvisited__node" ]
];