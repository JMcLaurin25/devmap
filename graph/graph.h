#ifndef GRAPH_H
#define GRAPH_H

#include <stdbool.h>
#include <stdlib.h>

#include "llist.h"
#include "decode.h"

struct edge {
	int weight;
	struct node *dst;
	struct edge *next;
};

struct node {
	struct device *data;
	struct edge *edges;
	struct node *next;
};

struct _adjllist_graph {
	struct node *nodes;
};

typedef struct _adjllist_graph graph;

graph *graph_create(void);
graph *graph_copy(graph *g);
void graph_disassemble(graph *g);
void graph_destroy(graph *g);

bool graph_add_node(graph *g, struct device *data);
bool graph_add_edge(graph *g, struct device *src, struct device *dst);

bool graph_has_node(graph *g, struct device *data);

bool graph_remove_node(graph *g, struct device *data);
bool graph_remove_edge(graph *g, size_t src, size_t dst);

void graph_reset_edges(graph *g);

size_t graph_node_count(graph *g);
size_t graph_edge_count(graph *g);

int graph_edge_weight(graph *g, void *from, size_t to);

struct llist *graph_adjacent_to(graph *g, void *data); 

void dev_graph_print(graph *g);
void graph_print(graph *g, void dst_print(void *, bool is_node));

bool battery_update(graph *g, size_t dev_id, size_t pwr_lvl);
void battery_print(graph *g, size_t level);

void graph_remove_print(graph *g, size_t dev_id);

void process_status_pkt(graph *g, struct meditrik *m);
void process_gps_pkt(graph *dev_graph, struct meditrik *data_meditrik);

#endif
