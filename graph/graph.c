
#include <stdlib.h>

#include "graph.h"
#include "path_find.h"

/*
For a thorough documentation see /docs/index.html
*/

/*!\file
	Graph functions were built off of foundational exercises
	in datastructure lessons. 
	https://github.com/umbctraining/datastructures/tree/master/graph/
 */

void __strip_node_edges(struct node *v);

//!Creates new graph
graph *graph_create(void)
{
	graph *g = malloc(sizeof(*g));
	if (!g) {
		return NULL;
	}
	g->nodes = NULL;

	return g;
}

//!Returns a duplicate of target graph
graph *graph_copy(graph *g)
{
	if (!g) {
		return NULL;
	}

	graph *copy = graph_create();
	if (!copy) {
		return NULL;
	}

	struct node *g_cur = g->nodes;
	while (g_cur) {
		graph_add_node(copy, g_cur->data);
		struct edge *g_edge = g_cur->edges;
		while (g_edge) {
			graph_add_edge(copy, g_cur->data, g_edge->dst->data);
			g_edge = g_edge->next;
		}
		g_cur = g_cur->next;
	}

	return copy;
}

//!Destroys the links of the graph, maintaining the data
void graph_disassemble(graph *g)
{
	if (!g) {
		return;
	}

	struct node *cur = g->nodes;
	while (cur) {
		struct edge *e = cur->edges;
		while (e) {
			struct edge *tmp = e->next;
			free(e);

			e = tmp;
		}

		struct node *tmp = cur->next;
		free(cur);
		cur = tmp;
	}

	free(g);
}

//!Destroys the links and the data of the graph
void graph_destroy(graph *g)
{
	if (!g) {
		return;
	}

	struct node *cur = g->nodes;
	while (cur) {
		struct edge *e = cur->edges;
		while (e) {
			struct edge *tmp = e->next;
			free(e);
			e = tmp;
		}

		struct node *tmp = cur->next;
		free((void*)cur->data);
		free(cur);
		cur = tmp;
	}

	free(g);
}

//!Add device to the graph
bool graph_add_node(graph *g, struct device *data)
{
	if (!g) {
		return false;
	}

	struct node *exist_node = g->nodes;
	while (exist_node) {
		if (exist_node->data->dev_id == data->dev_id) {
			//Change GPS coordinates
			exist_node->data->longitude = data->longitude;
			exist_node->data->latitude = data->latitude;
			exist_node->data->altitude = data->altitude;
			return false;
		}
		exist_node = exist_node->next;
	}

	struct node *new_node = malloc(sizeof(*new_node));
	if (!new_node) {
		return false;
	}

	new_node->data = data;
	new_node->edges = NULL;
	new_node->next = g->nodes;

	g->nodes = new_node;
	return true;
}

//!Add edge between nodes of a graph
bool graph_add_edge(graph *g, struct device *src, struct device *dst)
{
	if (!g) {
		return false;
	}
	struct node *node_src = NULL, *node_dst = NULL;
	// Find the two nodes; it is allowed for them to be the same node
	struct node *cur = g->nodes;
	while (cur && !(node_src && node_dst)) {
		if (cur->data->dev_id == src->dev_id) {
			node_src = cur;
		}
		if (cur->data->dev_id == dst->dev_id) {
			node_dst = cur;
		}
		cur = cur->next;
	}
	if (!(node_src && node_dst)) {
		return false;
	}

	// Check if an edge already exists between the two
	struct edge *check = node_src->edges;
	while (check) {
		if (check->dst == node_dst) {
			check->weight = 1;
			return true;
		}
		check = check->next;
	}

	// Add new edge 
	struct edge *new_edge = malloc(sizeof(*new_edge));
	if (!new_edge) {
		return false;
	}
	new_edge->weight = 1;
	new_edge->dst = node_dst;
	new_edge->next = node_src->edges;

	// Add reverse edge
	struct edge *new_rev_edge = malloc(sizeof(*new_edge));
	if (!new_rev_edge) {
		return false;
	}
	new_rev_edge->weight = 1;
	new_rev_edge->dst = node_src;
	new_rev_edge->next = node_dst->edges;

	node_src->edges = new_edge;
	node_dst->edges = new_rev_edge;


	return true;
}

//!Return (True/False) of existence of device in graph
bool graph_has_node(graph *g, struct device *data)
{
	if (!g) {
		return false;
	}

	struct node *cur = g->nodes;
	while (cur) {
		if (cur->data == data) {
			return true;
		}

		cur = cur->next;
	}

	return false;
}

//!Remove node from graph. 
//Function not currently utilized
bool graph_remove_node(graph *g, struct device *data)
{
	if (!g) {
		return false;
	}

	bool removed = false;
	struct node *cur = g->nodes;
	if (!cur) {
		return removed;
	} else if (cur->data == data) {
		g->nodes = cur->next;
		__strip_node_edges(cur);
		free(cur);
		removed = true;
	}

	struct node *prev = cur;
	cur = cur->next;
	while (cur) {
		if (cur->data == data) {
			prev->next = cur->next;
			__strip_node_edges(cur);
			free(cur);
			removed = true;
		}

		struct edge *check = cur->edges;
		if (!check) {
			break;
		} else if (check->dst->data == data) {
			cur->edges = check->next;
			free(check);
			break;
		}

		while (check && check->next) {
			if (check->next->dst->data == data) {
				struct edge *to_remove = check->next;
				check->next = to_remove->next;
				free(to_remove);
				break;
			}

			check = check->next;
		}

		prev = cur;
		cur = cur->next;
	}

	return removed;
}

//NOT CURRENTLY USED. To be utilized for Solving Suurballes Algorithm
void graph_reset_edges(graph *g)
{
	if (!g) {
		return;
	}

	struct node *cur_node = g->nodes;

	while (cur_node) {
		struct node *next_node = cur_node->next;
		while (next_node) {


			next_node = next_node->next;
		}
		cur_node = cur_node->next;
	}


}

//!Remove edge from graph
bool graph_remove_edge(graph *g, size_t src, size_t dst)
{
	if (!g || !src || !dst) {
		return false;
	}
	struct node *found_src = NULL;

	struct node *cur_node = g->nodes;
	//find source node
	while (cur_node) {
		if (cur_node->data->dev_id == src) {
			found_src = cur_node;
			break;
		}
		cur_node = cur_node->next;
	}
	//Remove if first node is the previous
	struct edge *cur_edge = found_src->edges;
	if (cur_edge->dst->data->dev_id == dst) {
		struct edge *temp = cur_edge;
		found_src->edges = cur_edge->next;
		free(temp);
		return true;
	}

	//For all subsequent nodes
	while (cur_edge->next) {
		if (cur_edge->next->dst->data->dev_id == dst) {
			struct edge *temp = cur_edge->next;
			cur_edge->next = cur_edge->next->next;
			free(temp);
			return true;
		}
		cur_edge = cur_edge->next;
	}

	return false;
}

//!Returns the node count of a graph
size_t graph_node_count(graph *g)
{
	if (!g) {
		return 0;
	}

	size_t count = 0;
	struct node *cur = g->nodes;
	while (cur) {
		++count;
		cur = cur->next;
	}

	return count;
}

//!Returns the edge count of a graph
size_t graph_edge_count(graph *g)
{
	if (!g) {
		return 0;
	}

	size_t count = 0;
	struct node *cur = g->nodes;
	while (cur) {
		struct edge *check = cur->edges;
		while (check) {
			++count;
			check = check->next;
		}
		cur = cur->next;
	}

	return count;
}

//!Returns particular edge weight between two nodes
int graph_edge_weight(graph *g, void *from, size_t to)
{
	if (!g) {
		return 0;
	}

	//Find the src device
	struct node *cur = g->nodes;
	while (cur && cur->data != from) {
		cur = cur->next;
	}
	if (!cur) {
		return 0;
	}

	//Find the dst device
	struct edge *check = cur->edges;
	while (check && check->dst->data->dev_id == to) {
			check = check->next;
	}
	if (!check) {
		return 0;
	}

	//Returns the edges weight, which is default of 1
	return check->weight;
}

//!Returns a list of adjacent nodes to a device
struct llist *graph_adjacent_to(graph *g, void *data) 
{
	if (!g || !data) {
		return NULL;
	}
	struct node *cur = g->nodes;
	while (cur) {
		if (cur->data->dev_id == *(size_t *)data) {
			if (!cur->edges) {
				return NULL;
			}

			struct llist *head = NULL;
			struct edge *cur_edge = cur->edges;
			while (cur_edge) {
				if (!head) {
					head = ll_create(cur_edge->dst->data->dev_id);
				} else {
					ll_add(&head, cur_edge->dst->data->dev_id);
				}
				cur_edge = cur_edge->next;
			}

			return head;
		}
		cur = cur->next;
	}

	return NULL;
}


void graph_print(graph *g, void to_print(void *, bool is_node))
{
	if (!g) {
		return;
	}

	struct node *cur = g->nodes;
	while (cur) {
		to_print(cur->data, true);

		struct edge *check = cur->edges;
		while (check) {
			to_print(check->dst->data, false);
			check = check->next;
		}

		cur = cur->next;
	}
}

//!Prints all elements and their edges from graph
void dev_graph_print(graph *g)
{
	if (!g) {
		return;
	}

	struct node *cur = g->nodes;
	while (cur) {
		printf("%lu", cur->data->dev_id);
		struct edge *cur_edge = cur->edges;
		while (cur_edge) {
			if (cur_edge->dst) {
				printf(u8" → %lu", cur_edge->dst->data->dev_id);
			}
			cur_edge = cur_edge->next;
		}

		printf("\n");
		cur = cur->next;
	}
}

//!Removes nodes edges.
void __strip_node_edges(struct node *v)
{
	struct edge *e = v->edges;
	while (e) {
		struct edge *tmp = e->next;
		free(e);

		e = tmp;
	}
}

//!Returns an individual devices edge count
static int node_edge_count(struct node *n)
{
	if (!n) {
		return 0;
	}
	int count = 0;
	struct edge *cur_edge = n->edges;
	while (cur_edge) {
		count++;
		cur_edge = cur_edge->next;
	}
	return count;
}

//!Updates received battery status of existing device
bool battery_update(graph *g, size_t dev_id, size_t pwr_lvl)
{
	if (!g || !dev_id || !pwr_lvl) {
		return false;
	}

	struct node *cur_node = g->nodes;
	while (cur_node) {
		if (cur_node->data->dev_id == dev_id) {
			cur_node->data->batt_pwr = pwr_lvl;
			return true;
		}
		cur_node = cur_node->next;
	}
	return false;
}

//!Prints the low battery status.
void battery_print(graph *g, size_t level)
{
	if (!g) {
		return; 
	}

	struct node *cur_node = g->nodes;
	struct llist *low_batt = NULL;
	struct device *cur_dev = NULL;

	while (cur_node) {
		if (!(cur_dev = cur_node->data)) {
			return;
		} 
		
		if ((cur_dev->batt_pwr > 0) && (cur_dev->batt_pwr < level)) {
			if (!low_batt) {
				low_batt = ll_create(cur_node->data->dev_id);
			} else {
				ll_add(&low_batt, cur_node->data->dev_id);
			}
		}
		cur_node = cur_node->next;
	}

	if (low_batt) {
		struct llist *head = low_batt;
		printf("\nLow battery (%lu%s):\n", level, "%");
		while (low_batt) {
			printf("Device #%lu\n", low_batt->dev_id);
			low_batt = low_batt->next;
		}
		ll_destroy(head);
	}
}

//!Prints the devices that need to be removed
void graph_remove_print(graph *g, size_t dev_id)
{
	if (!g) {
		return;
	}

	struct node *cur = g->nodes;
	while (cur) {
		if (cur->data->dev_id == dev_id) {
			if (node_edge_count(cur) < 2) {
				printf("Remove device #%lu\n", cur->data->dev_id);
			}
		}
		cur = cur->next;
	}
}

//!Sets values for a battery status, when device is not apart of the network.
void process_status_pkt(graph *dev_graph, struct meditrik *data_meditrik)
{
	size_t cur_device = data_meditrik->header->src_dev;
	size_t batt_status = data_meditrik->payload->status->battery_pwr;
	if (!battery_update(dev_graph, cur_device, batt_status)) {
		//Add device without coordinates to graph
		struct device *battery = malloc(sizeof(*battery));
		if (battery) {
			battery->dev_id = cur_device;
			battery->batt_pwr = batt_status;
			battery->longitude = 0;
			battery->latitude = 0;
			battery->altitude = 0;

			if (graph_add_node(dev_graph, battery) == false) {
				free(battery);
			}
		}
	}
}

//!Processes a GPS packet to store elements
void process_gps_pkt(graph *dev_graph, struct meditrik *data_meditrik)
{
	struct device *new_dev = make_device(data_meditrik);
	if (!new_dev) {
		return;
	}

	if (graph_add_node(dev_graph, (void *)new_dev) == false) {
		free(new_dev);
		return;	
	}

	struct node *cur_node = dev_graph->nodes;
	while (cur_node) {
		struct device *exist_dev = cur_node->data;
		double cur_dist = device_distance(new_dev, exist_dev);

		if (cur_dist > 1.25 * 0.3048 && cur_dist <= 5.0) {
			graph_add_edge(dev_graph, new_dev, exist_dev);
		}

		cur_node = cur_node->next;
	}
}
