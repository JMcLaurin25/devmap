
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "decode.h"
#include "graph.h"
#include "path_find.h"

/*
For a thorough documentation see /docs/index.html
*/

int main (int argc, char *argv[])
{
	size_t pwr_warn = 5.00;//Default power warning level @file

	//! Argument parsing utilizing getopt.
	int arg_option = 0;
	while ((arg_option = getopt(argc, argv, "P:p:")) != -1) {
		switch (arg_option) {
			case 'P':
			case 'p':
				sscanf(optarg, "%lu", &pwr_warn);
				if (pwr_warn > 100) {
					printf("Power level provided is invalid. Defaults to 5%s\n", "%");
					pwr_warn = 5.00;
				}
				break;
			case '?':
				printf("No power level provided with [-p option]\n");
				return 1;
				break;
		}
	}

	graph *dev_graph = graph_create();
	if (!dev_graph) {
		return 1;
	}

	//! Processing of .pcap files in a loop.
	for (int index = optind; index < argc; index++) {
		int offset_header_start = 54, packet_header_len = 30, more_data = 1, end_of_packet = 0, eof_pos;

		FILE *pcap_file;
		char *pcap_path;
 		pcap_path = strdup(argv[index]);
	
		pcap_file = fopen(pcap_path, "rb");
		if (!pcap_file) {
			fprintf(stderr, "\tUnable to read file: -%s-\n", pcap_path);
			free(pcap_path);
			return 1;
		} else {
			fseek(pcap_file, 36, 1);
			fread(&end_of_packet, 1, 1, pcap_file);
			end_of_packet += (int)ftell(pcap_file) + 2;
			rewind(pcap_file);
			fseek(pcap_file, -1, SEEK_END);
			eof_pos = ftell(pcap_file);
			rewind(pcap_file);
		}

		struct meditrik *data_meditrik = make_meditrik(); //Make meditrik structure
		if (!data_meditrik) {
			fprintf(stderr, "Creation of Meditrik structure failed.\n");
			free(pcap_path);
			return 1;
		}


		while (more_data) {

			if (!check_ip(pcap_file, &offset_header_start)) {
				goto NEXT_PKT;
			}

			if (!decode_in(data_meditrik, pcap_file, &offset_header_start)) {
				fprintf(stderr, "Error reading file '%s'.\n", pcap_path);
				free_meditrik_payload(data_meditrik);
				goto NEXT_PKT;
			}
			if (!set_payload(data_meditrik, pcap_file, &offset_header_start)) {
				fprintf(stderr, "Setting of payload failed.\n");
				free_meditrik_payload(data_meditrik);
				goto NEXT_PKT;
			}


			//!Check version and type. 0: return battery status. 2: GPS data.
			if (data_meditrik->header->version != 1) {
				free_meditrik_payload(data_meditrik);
				goto NEXT_PKT;
			}
			//!Processing of created .pcap files to the network graph
			switch (data_meditrik->header->type) {
				case 0:
					process_status_pkt(dev_graph, data_meditrik);
					break;
				case 2:
					process_gps_pkt(dev_graph, data_meditrik);
					break;
				default:
					break;
			}
			free_meditrik_payload(data_meditrik);

		NEXT_PKT:
			//Find end of next packet
			rewind(pcap_file);
			offset_header_start = end_of_packet + packet_header_len + 1;

			fseek(pcap_file, end_of_packet + 9, 1);
			fread(&end_of_packet, 1, 1, pcap_file);
			end_of_packet += (int)ftell(pcap_file) + 2;

			if (end_of_packet >= eof_pos) {
				more_data = 0;
			} 

			memset(data_meditrik->header, 0, sizeof(*data_meditrik->header));
		}

		free_meditrik(data_meditrik);
		fclose(pcap_file);
		free(pcap_path);
	}

	//!Graph modification based on vendor specifications.
	struct llist *head_removal_list = find_single_dev(dev_graph);
	struct llist *removal_list = head_removal_list;

	size_t node_count = graph_node_count(dev_graph);

	if (node_count == 0) {
		printf("There are no devices present in your network\n");
	} else if ((ll_count(removal_list) > (node_count / 2)) && (node_count > 3)) {
		printf("Too many Network Alterations needed\n");
	} else {
		if (removal_list) {
			printf("Network Alterations:\n");
			while (removal_list) {
				graph_remove_print(dev_graph, removal_list->dev_id);
				removal_list = removal_list->next;
			}
		} else if (!removal_list && (node_count > 2)) {
			printf("Network satisfies vendor recommendations\n");
		}
	}

	//!Output of results
	ll_destroy(head_removal_list);
	battery_print(dev_graph, pwr_warn);

	graph_destroy(dev_graph);
}
