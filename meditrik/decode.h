#ifndef DECODE_H
#define DECODE_H

#include <stdbool.h>
#include <stdio.h>

struct device {
	size_t dev_id;
	double batt_pwr;
	double longitude, latitude;
	float altitude;
};

struct meditrik {
	struct pkt_format *header;
	union data_payload *payload;
};

struct pkt_format {
	unsigned int version;
	unsigned int sq_id;
	unsigned int type;
	unsigned int total_length;
	unsigned int src_dev;
	unsigned int dst_dev;
};

union data_payload {
	struct status_payload *status;
	struct command_payload *command;
	struct gps_payload *gps;
	struct message_payload *message;
};

struct status_payload { //Status of Device - type_0
	union {
		double battery_pwr;
		unsigned char battery_array[sizeof(double)];
	};
	unsigned int glucose;
	unsigned int capsaicin;
	unsigned int omorfine;
};

struct command_payload { //Command Instruction - type_1
	unsigned int command;
	unsigned int parameter;
};

struct gps_payload { //GPS Data - type_2
	union {
		double longitude;
		unsigned char long_array[sizeof(double)];
	};
	union {
		double latitude;
		unsigned char lat_array[sizeof(double)];
	};
	union {
		float alt_value;
		char alt_array[sizeof(float)];
	};
};

struct message_payload { //Message - type_3
	char message[1024];
};

struct device *make_device(struct meditrik *data);
struct meditrik *make_meditrik(void);

int decode_in(struct meditrik *meditrik, FILE *pcap_file, int *offset_start);
bool check_ip(FILE *file, int *offset);
int set_payload(struct meditrik *meditrik_data, FILE *pcap_file, int *offset_start);

int decode_out(struct meditrik *);
void free_meditrik(struct meditrik *data_meditrik);
void free_meditrik_payload(struct meditrik *data);

int ip_version(FILE *pcap_file, int offset);
int port_num(FILE *pcap_file, int offset);

#endif
