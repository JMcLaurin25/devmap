#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "decode.h"

/*
For a thorough documentation see /docs/index.html
*/

static int get_version(struct pkt_format *header, FILE *pcap_file, int *offset_start);
static int get_sq_id(struct pkt_format *header, FILE *pcap_file, int *offset_start);
static int get_type(struct pkt_format *header, FILE *pcap_file, int *offset_start);
static int get_length(struct pkt_format *header, FILE *pcap_file, int *offset_start);
static int get_src(struct pkt_format *header, FILE *pcap_file, int *offset_start);
static int get_dst(struct pkt_format *header, FILE *pcap_file, int *offset_start); 

static int get_dbl_data(double *var, FILE *pcap, int location, int length);
static int get_uint_data(unsigned int *var, FILE *pcap, int location, int length, int end);
static int get_alt_data(char *alt_array, FILE *pcap, int location, int length);
static int get_message(struct meditrik *data, FILE *pcap, int location);

/*!\file
Creates new device structure. Setting variables from the meditrik packets
*/
struct device *make_device(struct meditrik *data)
{
	if (!data) {
		return NULL;
	}

	struct device *new_dev = malloc(sizeof(*new_dev));
	if (new_dev) {
		new_dev->dev_id = data->header->src_dev; 
		new_dev->batt_pwr = -1;
		new_dev->longitude = data->payload->gps->longitude;
		new_dev->latitude = data->payload->gps->latitude;
		new_dev->altitude = (data->payload->gps->alt_value * 6) * 0.3048; //Conversion to meters

		return new_dev;
	}
	return NULL;
}

//!Creates medtrik packet from .pcap files
struct meditrik *make_meditrik(void)
{
	struct meditrik *meditrik = malloc(sizeof(*meditrik));
	if (!meditrik) {
		return NULL;
	}

	meditrik->header = malloc(sizeof(*meditrik->header));
	if (!meditrik->header) {
		return NULL;
	}

	memset(meditrik->header, 0, sizeof(*meditrik->header));

	meditrik->payload = malloc(sizeof(*meditrik->payload));
	if (!meditrik->payload) {
		return NULL;
	}

	return meditrik;
}

//!Retrieves the Meditrik packet header fields
int decode_in(struct meditrik *meditrik, FILE *pcap_file, int *offset_start)// Get the packet format information
{
	if (!meditrik) {
		printf("NULL Meditrik structure passed to function set_payload.\n");
		return 0;
	} else if (!pcap_file) {
		printf("Unable to read file in function set_payload\n");
		return 0;
	} else if (!offset_start) {
		printf("Invalid offset passed to function set_payload\n");
		return 0;
	}

	//Get values
	if (!get_version(meditrik->header, pcap_file, offset_start)) {
		return 0;
	}
	if (!get_sq_id(meditrik->header, pcap_file, offset_start)) {
		return 0;
	}
	if (!get_type(meditrik->header, pcap_file, offset_start)) {
		return 0;
	}
	if (!get_length(meditrik->header, pcap_file, offset_start)) {
		return 0;
	}
	if (!get_src(meditrik->header, pcap_file, offset_start)) {
		return 0;
	}
	if (!get_dst(meditrik->header, pcap_file, offset_start)) {
		return 0;
	}

	return 1;
}

//!Verifies IPv4/IPv6 and port 57007
bool check_ip(FILE *file, int *offset)
{
	if (!file) {
		return false;
	}
	int ipv = 0;
	if ((ipv = ip_version(file, *offset)) == 4) {
		if (port_num(file, *offset + 20) == 57005) {
			*offset += 28;//IPv4(20) + protocol(8)
			return true;
		} else {
			return false;
		}
	} else if (ipv == 6) {
		if (port_num(file, *offset + 40) == 57005) {
			*offset += 48;//IPv6(40) + protocol(8)
			return true;
		} else {
			return false;
		}
	} 
	return false;
}

//!Sets payload fields based upon header designations
int set_payload(struct meditrik *data, FILE *pcap_file, int *offset_start)
{
	if (!data) {
		printf("NULL Meditrik structure passed to function set_payload.\n");
		return 0;
	} else if (!pcap_file) {
		printf("Unable to read file in function set_payload\n");
		return 0;
	} else if (!offset_start) {
		printf("Invalid offset passed to function set_payload\n");
		return 0;
	}

	switch (data->header->type) {
		case 0: //Status of Device
			data->payload->status = malloc(sizeof(*data->payload->status)); //Allocation of head memory
			if (!data->payload->status) {
				printf("Allocation of memory for payload failed in function, set_payload.\n");
				return 0;
			} else {
				memset(data->payload->status, 0, sizeof(*data->payload->status));
			}

			if (!get_dbl_data(&data->payload->status->battery_pwr, pcap_file, *offset_start + 12, 8)) {
				return 0;
			}
			data->payload->status->battery_pwr *= 100;
			if (!get_uint_data(&data->payload->status->glucose, pcap_file, *offset_start + 20, 2, 1)) {
				return 0;
			}
			if (!get_uint_data(&data->payload->status->capsaicin, pcap_file, *offset_start + 22, 2, 1)) {
				return 0;
			}
			if (!get_uint_data(&data->payload->status->omorfine, pcap_file, *offset_start + 24, 2, 1)) {
				return 0;
			}

			break;
		case 1: //Command Instruction
			data->payload->command = malloc(sizeof(*data->payload->command));
			if (!data->payload->command) {
				printf("Allocation of memory for payload failed in function, set_payload.\n");
				return 0;
			} else {
				memset(data->payload->command, 0, sizeof(*data->payload->command)); //Pack all bits to zero
			}

			if (!get_uint_data(&data->payload->command->command, pcap_file, *offset_start + 12, 2, 1)) {
				return 0;
			}
			if (data->payload->command->command % 2 != 0) {
				if (!get_uint_data(&data->payload->command->parameter, pcap_file, *offset_start + 14, 2, 1)) {
					return 0;
				}
			}
			break;
		case 2: //GPS Data
			data->payload->gps = malloc(sizeof(*data->payload->gps));
			if (!data->payload->gps) {
				printf("Allocation of memory for payload failed in function, set_payload.\n");
				return 0;
			} else {
				memset(data->payload->gps, 0, sizeof(*data->payload->gps));
			}

			if (!get_dbl_data(&data->payload->gps->longitude, pcap_file, *offset_start + 12, 8)) {
				return 0;
			}
			if (!get_dbl_data(&data->payload->gps->latitude, pcap_file, *offset_start + 20, 8)) {
				return 0;
			}
			if (!get_alt_data(data->payload->gps->alt_array, pcap_file, *offset_start + 28, 4)) {
				return 0;
			}

			break;
		case 3: //Message
			data->payload->message = malloc(sizeof(*data->payload->message));
			if (!data->payload->message) {
				printf("Allocation of memory for payload failed in function, set_payload.\n");
				return 0;
			} else {
				memset(data->payload->message, 0, sizeof(*data->payload->message));
			}

			if (!get_message(data, pcap_file, *offset_start + 12)) {
				return 0;
			}
			break;
		default:
			printf("Unknown Payload Type.\n");
			break;
	}
	return 1;
}

//!Prints the meditrik packets fields
int decode_out(struct meditrik *meditrik)
{
	if (!meditrik) {
		printf("NULL Meditrik structure passed to function decode_out.\n");
		return 0;
	}

	//test prints
	printf("Version: %d\n", meditrik->header->version);
	printf("Sequence ID: %d\n", meditrik->header->sq_id);
	printf("Source Device ID: %d\n", meditrik->header->src_dev);
	printf("Destination Device ID: %d\n", meditrik->header->dst_dev);

	switch(meditrik->header->type) {
		case 0:
			printf("Battery Power: %.2f%s\n", meditrik->payload->status->battery_pwr, "%");
			printf("Glucose: %d\n", meditrik->payload->status->glucose);
			printf("Capsaicin: %d\n", meditrik->payload->status->capsaicin);
			printf("Omorfine: %d\n", meditrik->payload->status->omorfine);
			break;
		case 1: //Command Payload

			switch (meditrik->payload->command->command) {
				case 0:
					printf("GET STATUS\n");
					break;
				case 1:
					printf("SET_GLUCOSE");
					break;
				case 2:
					printf("GET_GPS\n");
					break;
				case 3:
					printf("SET_CAPSAICIN");
					break;
				case 4:
					printf("RESERVED\n");
					break;
				case 5:
					printf("SET_OMORFINE");
					break;
				case 6:
					printf("RESERVED\n");
					break;
				case 7:
					printf("REPEAT");
					break;
				default:
					printf("Unknown command\n");
					break;
			}

			if (meditrik->payload->command->command % 2 != 0) {
				printf("=%d\n", meditrik->payload->command->parameter);
			}
			break;
		case 2:
			printf("Longitude: %.9f\n", meditrik->payload->gps->longitude);
			printf("Latitude: %.9f\n", meditrik->payload->gps->latitude);
			printf("Altitude: %.0f ft\n", meditrik->payload->gps->alt_value * 6);
			break;
		case 3:
			printf("Message: %s\n", meditrik->payload->message->message);
					break;
		default:
			printf("Unknown Payload Type.\n");
			break;
	}

	return 1;
}

void free_meditrik(struct meditrik *data)
{
	if (data->payload) {
		free(data->payload);
	}
	if (data->header) {
		free(data->header);
	}
	if (data) {
		free(data);
	}
}

void free_meditrik_payload(struct meditrik *data)
{
	switch (data->header->type) {
		case 0:
			free(data->payload->status);
			break;
		case 1:
			free(data->payload->command);
			break;
		case 2:
			free(data->payload->gps);
			break;
		case 3:
			free(data->payload->message);
			break;
		default:
			break;
	}
}

/*!
The following functions are utilized to retrieve particular fields from the .pcap
file and convert them to target formats.
*/
static int get_version(struct pkt_format *header, FILE *pcap_file, int *offset_start)
{
	if (!header) {
		printf("NULL Meditrik structure passed to function get_version.\n");
		return 0;
	}

	unsigned int vrs_data, mask = 15;

	rewind(pcap_file);

	fseek(pcap_file, *offset_start, 1);
	fread(&vrs_data, 1, 1, pcap_file);//Version

	vrs_data = vrs_data >> 4;

	vrs_data = mask & vrs_data;
	header->version = vrs_data; //Sets the Version

	return 1;
}

static int get_sq_id(struct pkt_format *header, FILE *pcap_file, int *offset_start)
{
	if (!header) {
		printf("NULL Meditrik structure passed to function get_sq_id.\n");
		return 0;
	}

	unsigned int seq, mask;
	rewind(pcap_file);

	fseek(pcap_file, *offset_start, 1);//Sequence
	fread(&seq, 2, 1, pcap_file);
	seq = htons(seq);

	seq = seq >> 3;

	mask = (1 << 9) - 1;
	seq = mask & seq;

	header->sq_id = seq;//Sets the Seq ID

	return 1;
}

static int get_type(struct pkt_format *header, FILE *pcap_file, int *offset_start)
{
	if (!header) {
		printf("NULL Meditrik structure passed to function get_type\n");
		return 0;
	} else if (!pcap_file) {
		printf("Unable to read file in function get_type\n");
		return 0;
	} else if (!offset_start) {
		printf("Invalid offset passed to function get_type\n");
		return 0;
	}

	rewind(pcap_file);
	unsigned int type, mask;

	fseek(pcap_file, *offset_start + 1, 1);//Sequence
	fread(&type, 1, 1, pcap_file);
	mask = (1 << 3) - 1;
	type = mask & type;

	header->type = type;//Sets the Type

	return 1;
}

static int get_length(struct pkt_format *header, FILE *pcap_file, int *offset_start)
{
	if (!header) {
		printf("NULL Meditrik structure passed to function get_length\n");
		return 0;
	} else if (!pcap_file) {
		printf("Unable to read file in function get_length\n");
		return 0;
	} else if (!offset_start) {
		printf("Invalid offset passed to function get_length\n");
		return 0;
	}

	rewind(pcap_file);
	unsigned int length;

	fseek(pcap_file, *offset_start + 2, 1);//Sequence
	fread(&length, 2, 1, pcap_file);
	length = htons(length);

	header->total_length = length;//Sets the Total Length

	return 1;
}

static int get_src(struct pkt_format *header, FILE *pcap_file, int *offset_start)
{
	if (!header) {
		printf("NULL Meditrik structure passed to function get_src\n");
		return 0;
	} else if (!pcap_file) {
		printf("Unable to read file in function get_src\n");
		return 0;
	} else if (!offset_start) {
		printf("Invalid offset passed to function get_src\n");
		return 0;
	}

	rewind(pcap_file);
	unsigned int src;

	fseek(pcap_file, *offset_start + 4, 1);//Sequence
	fread(&src, 4, 1, pcap_file);
	src = ntohl(src);

	header->src_dev = src;//Sets the Src ID

	return 1;
}

static int get_dst(struct pkt_format *header, FILE *pcap_file, int *offset_start)
{
	if (!header) {
		printf("NULL Meditrik structure passed to function get_dst\n");
		return 0;
	} else if (!pcap_file) {
		printf("Unable to read file in function get_dst\n");
		return 0;
	} else if (!offset_start) {
		printf("Invalid offset passed to function get_dst\n");
		return 0;
	}

	rewind(pcap_file);
	unsigned int dst;

	fseek(pcap_file, *offset_start + 8, 1);//Sequence
	fread(&dst, 4, 1, pcap_file);
	dst = htonl(dst);

	header->dst_dev = dst;//Sets the Dest ID

	return 1;
}

//------Local functions Below

/*
 * Function 'get_dbl_data' has variables:
 * double *var       : the location of the structures variable
 * FILE *pcap        : pcap file used in current process
 * int location      : byte location, from start, in the pcap file
 * int length        : byte length to read from pcap file at location
 */
static int get_dbl_data(double *var, FILE *pcap, int location, int length)
{
	if (!var) { //Argument checks
		printf("Invalid argument passed to function get_dbl_data.\n");
		return 0;
	} else if (!pcap) {
		printf("Unable to read file in function get_dbl_data\n");
		return 0;
	} else if (!location) {
		printf("Invalid offset passed to function get_dbl_data\n");
		return 0;
	} else if (!length) {
		printf("Invalid argument passed to function get_dbl_data.\n");
		return 0;
	}

	double temp_data;
	rewind(pcap);

	fseek(pcap, location, 1);
	fread(&temp_data, 1, length, pcap);

	*var = temp_data; //Sets the variable
	return 1;
}

/*
 * Function 'get_alt_data' has variables:
 * unsigned long *var: the location of the structures variable
 * FILE *pcap        : pcap file used in current process
 * int location      : byte location, from start, in the pcap file
 * int length        : byte length to read from pcap file at location
 */
static int get_alt_data(char *alt_array, FILE *pcap, int location, int length)
{
	if (!alt_array) { //Argument checks
		printf("Invalid argument passed to function get_alt_data.\n");
		return 0;
	} else if (!pcap) {
		printf("Unable to read file in function get_alt_data\n");
		return 0;
	} else if (!location) {
		printf("Invalid offset passed to function get_alt_data\n");
		return 0;
	} else if (!length) {
		printf("Invalid argument passed to function get_alt_data.\n");
		return 0;
	}

	rewind(pcap);

	int index;
	fseek(pcap, location, 1);

	for (index = 0; index < length; index++) {
		alt_array[index] = fgetc(pcap); //Sets the variable
	}
	if (alt_array == NULL) {
		return 0;
	}
	alt_array[index] = '\0';

	return 1;
}

/*
 * Function 'get_uint_data' has variables:
 * unsigned int *var : the location of the structures variable
 * FILE *pcap        : pcap file used in current process
 * int location      : byte location, from start, in the pcap file
 * int length        : byte length to read from pcap file at location
 * int end           : 0 = maintain endianess, 1 = htons
 */
static int get_uint_data(unsigned int *var, FILE *pcap, int location, int length, int end)
{
	if (!var) { //Argument checks
		printf("Invalid argument passed to function get_uint_data.\n");
		return 0;
	} else if (!pcap) {
		printf("Unable to read file in function get_uint_data\n");
		return 0;
	} else if (!location) {
		printf("Invalid offset passed to function get_uint_data\n");
		return 0;
	} else if (!length) {
		printf("Invalid argument passed to function get_uint_data.\n");
		return 0;
	} else if (!end) {
		printf("Invalid argument passed to function get_uint_data.\n");
		return 0;
	}

	unsigned int temp_data;
	rewind(pcap);

	fseek(pcap, location, 1);
	fread(&temp_data, 1, length, pcap);

	if (end == 1) {
		*var = htons(temp_data); //Sets the variable
	} else if (end == 0) {
		*var = temp_data; //Sets the variable
	}
	return 1;
}

static int get_message(struct meditrik *data, FILE *pcap, int location)
{
	if (!data) { //Argument checks
		printf("Invalid argument passed to function get_message.\n");
		return 0;
	} else if (!pcap) {
		printf("Unable to read file in function get_message\n");
		return 0;
	} else if (!location) {
		printf("Invalid offset passed to function get_message\n");
		return 0;
	}

	rewind(pcap);

	int sz_message = data->header->total_length - 12, index;
	fseek(pcap, location, 1);

	for (index = 0; index < sz_message; index++) {
		data->payload->message->message[index] = fgetc(pcap);
		
	}
	if (data->payload->message->message == NULL) {
		return 0;
	}
	data->payload->message->message[index] = '\0';
	return 1;
}

//!Returns the .pcap files IP version field
int ip_version(FILE *pcap_file, int offset)
{
	if (!pcap_file) {
		printf("Unable to read file in function ip_version\n");
		return 0;
	} else if (!offset) {
		printf("Invalid offset passed to function ip_version\n");
		return 0;
	}

	int temp = 0;
	rewind(pcap_file);
	fseek(pcap_file, offset, 1);
	fread(&temp, 1, 1, pcap_file);

	temp = temp >> 4;
	temp = temp & 15;

	rewind(pcap_file);
	return temp;
}

//!Returns the .pcap files Port number field
int port_num(FILE *pcap_file, int offset)
{
	if (!pcap_file) {
		printf("Unable to read file in function ip_version\n");
		return 0;
	} else if (!offset) {
		printf("Invalid offset passed to function ip_version\n");
		return 0;
	}

	int temp = 0;

	fseek(pcap_file, offset, 1);
	fread(&temp, 2, 1, pcap_file);

	temp = htons(temp);
	rewind(pcap_file);
	return temp;
}
