
CFLAGS+=-std=c11 -D_GNU_SOURCE
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline -fstack-usage

CFLAGS+=-Imeditrik -Igraph -Ihash -Iheap -Illist -Ipath_finding -Iqueue
LDFLAGS+=-Lmeditrik -Lgraph -Lhash -Lheap -Lllist -Lpath_finding -Lqueue

devmap: devmap.o meditrik/decode.o graph/graph.o hash/hash.o heap/heap.o llist/llist.o path_finding/path_find.o queue/queue.o -lm


.PHONY: clean debug profile

clean:
	-rm *.o */*.o *.su */*.su devmap

debug: CFLAGS+=-g
debug: devmap

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: devmap

