
#include <stdlib.h>
#include <stdio.h>

#include "llist.h"

/*
For a thorough documentation see /docs/index.html
*/

/*!\file
	Linked list functions were built off of foundational exercises
	in datastructure lessons. 
	https://github.com/umbctraining/datastructures/tree/master/llist/
 */

/*!
Mallocs memory for linked list and sets values.
*/
struct llist *ll_create(size_t data)
{
	struct llist *new_list = malloc(sizeof(*new_list));
	if (!new_list) {
		return NULL;
	}
	//!Device ID
	new_list->dev_id = data;
	new_list->next = NULL;
	return new_list;
}

/*!
Walks the linked list and frees the connection, maintaining the data.
*/
void ll_disassemble(struct llist *l)
{
	while(l) {
		struct llist *temp = l->next;
		free(l);
		l = temp;
	}
}

/*!
Walks the linked list and frees them one by one.
*/
void ll_destroy(struct llist *l)
{
	while(l) {
		struct llist *temp = l->next;
		free(l);
		l = temp;
	}
}

/*!
Adds a new device ID to the linked list.
*/
void ll_add(struct llist **l, size_t data)
{
	struct llist *new_node = ll_create(data);
	if (new_node) {
		new_node->next = *l;
		*l = new_node;
	}
}

/*!
Removes a node from the linked list.
*/
size_t ll_remove(struct llist **l)
{
	struct llist *old_node = *l;
	*l = old_node->next;
	size_t data = old_node->dev_id;
	free(old_node);
	return data;
}

/*!
Verifies the existence of an ID in the linked list.
*/
bool ll_fetch(struct llist *l, size_t data)
{
	if (!l || !data) {
		return false;
	}
	
	while (l) {
		if (l->dev_id == data) {
			return true;
		}
		l = l->next;
	}
	return false;
}

/*!
Combines two linked lists.
*/
void ll_append(struct llist *a, struct llist *b)
{
	while (a->next) { //Find end of llist
		a = a ->next;
	}
	a->next = b;
}

/*!
Returns the item count of the list.
*/
size_t ll_count(struct llist *l)
{
	if (l) {
		size_t count = 0;

		while (l) {
			count++;
			l = l->next;
		}
		return count;
	}
	return 0;
}

/*!
Prints the linked list with battery status formatting.
*/
void batt_ll_print(struct llist *a)
{
	while (a) {
		printf("Device #%lu\n", a->dev_id);
		a = a->next;
	}
}

