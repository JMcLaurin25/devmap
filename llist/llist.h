#ifndef LLIST_H
#define LLIST_H

#include <stdbool.h>

struct llist {
	//! Device ID
	size_t dev_id;
	struct llist *next;
};

struct llist *ll_create(size_t data);
void ll_disassemble(struct llist *l);
void ll_destroy(struct llist *l);

void ll_add(struct llist **l, size_t data);
size_t ll_remove(struct llist **l);
bool ll_fetch(struct llist *l, size_t data);

void ll_append(struct llist *a, struct llist *b);

size_t ll_count(struct llist *l);

void batt_ll_print(struct llist *a);
void batt_ll_destroy(struct llist *b);

#endif
