
#ifndef HASH_H
#define HASH_H

#include <stdbool.h>
#include <stdlib.h>

#include "decode.h"

struct h_llist {
	size_t dev_id;
	void *h_data;
	struct h_llist *next;
};

struct _hash {
	size_t item_count;
	size_t capacity;
	struct h_llist **list;
};

typedef struct _hash hash;

hash *hash_create(void);
void hash_destroy(hash *h);
void hash_disassemble(hash *h);

bool hash_insert(hash *h, size_t dev_id, void *data);
void *hash_fetch(hash *h, size_t dev_id);
bool hash_exists(hash *h, size_t data);

void hash_print(hash *h);

#endif
