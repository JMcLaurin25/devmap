
#include <stdint.h>
#include <string.h>

#include "path_find.h"
#include "hash.h"

/*
For a thorough documentation see /docs/index.html
*/

/*!\file
	Hash functions were built off of foundational exercises
	in datastructure lessons. 
	https://github.com/umbctraining/datastructures/tree/master/hash/
 */

//Static funciton definitions
static hash *__hash_create(size_t capacity);
static uint64_t __wang_hash(uint64_t key);
static void h_llist_disassemble(struct h_llist *list);
static void h_llist_destroy(struct h_llist *list);
static struct h_llist *h_llist_create(size_t dev_id, void *data);
static size_t hashish(size_t dev_id, size_t capacity);
static int hash_recalculate(hash *h);

/*!
Creates a new hash using a default size of 8
*/
hash *hash_create(void)
{
	static const size_t DEFAULT_SIZE = 8;
	return __hash_create(DEFAULT_SIZE);
}

/*!
Iterates through a hash and destroys each list at each index
*/
void hash_destroy(hash *h)
{
	if (!h) {
		return;
	}

	for (size_t n=0; n < h->capacity; ++n) {
		h_llist_destroy(h->list[n]);
	}
	free(h->list);
	free(h);
}


/*!
Iterates through a hash and disassembles the data link at each index
*/
void hash_disassemble(hash *h)
{
	if (!h) {
		return;
	}

	for (size_t n=0; n < h->capacity; ++n) {
		h_llist_disassemble(h->list[n]);
	}

	free(h->list);
	free(h);
}

/*!
Inserts device information into a hash
*/
bool hash_insert(hash *h, size_t dev_id, void *data)
{
	if (!h || !data) {
		return false;
	}
	size_t index = hashish(dev_id, h->capacity);
	// Search the hash for duplicate
	struct h_llist *temp = h->list[index];
	while (temp) {
		if (temp->dev_id == dev_id) {
			//free(data);
			return false;
		}
		temp = temp->next;
	}

	// If the load factor is hit, rebuild the hash,
	// and recalculate the index
	int val = hash_recalculate(h);
	if (val < 0) {
		//free(data);
		return false;
	} else if (val > 0) {
		index = hashish(dev_id, h->capacity);
	}

	struct h_llist *new = h_llist_create(dev_id, data); 
	if (!new) {
		//free(data);
		return false;
	}

	// Add new value to head of linked list
	new->next = h->list[index];
	h->list[index] = new;
	h->item_count += 1;

	return true;
}

/*!
Returns a device from the hash
*/
void *hash_fetch(hash *h, size_t dev_id)
{
	if (!h || !dev_id) {
		return NULL;
	}

	for (size_t index = 0; index < h->capacity; index++) {
		struct h_llist *cur= h->list[index];
		while (cur) {
			if (cur->dev_id == dev_id) {
				return cur->h_data;
			}
			cur = cur->next;
		}
	}

	return NULL;
}

/*!
Verify that a device exists in a hash (True/False)
*/
bool hash_exists(hash *h, size_t data) 
{
	if (!h || !data) {
		return false;
	}

	for (size_t index = 0; index < h->capacity; index++) {
		struct h_llist *cur = h->list[index];
		while (cur) {
			struct visited_node *v_node = cur->h_data;
			size_t cur_id = v_node->priority->dev_id;
			if (cur_id) {
				if (cur_id == data) {
					return true; 
				}
			}
			cur = cur->next;
		}
	}

	return false;
}

//Static functions below
//!Rebalances the hash table when capacity exceeds 70%
static int hash_recalculate(hash *h) 
{
	static const double LOAD_FACTOR = 0.7;
	if (h->item_count < LOAD_FACTOR * h->capacity) {
		return 0;
	}

	hash *copy = __hash_create(h->capacity * 2);
	if (!copy) {
		return -2;
	}

	for (size_t n=0; n < h->capacity; ++n) {
		struct h_llist *temp = h->list[n];
		while (temp) {
			hash_insert(copy, temp->dev_id, temp->h_data);
			temp = temp->next;
		}
	}

	for (size_t n=0; n < h->capacity; ++n) {
		h_llist_disassemble(h->list[n]);
	}
	free(h->list);

	h->capacity = copy->capacity;
	h->item_count = copy->item_count;
	h->list = copy->list;
	free(copy);

	return 1;
}

//!Creates a new hash using a 'capacity' size
static hash *__hash_create(size_t capacity) 
{
	hash *h = malloc(sizeof(*h));
	if (!h) {
		return NULL;
	}

	h->list = calloc(capacity, sizeof(*h->list));
	if (!h->list) {
		free(h);
		return NULL;
	}

	h->capacity = capacity;
	h->item_count = 0;
	return h;
}

/*!
Hashing Algorithm - Wang Hash
https://naml.us/blog/tag/thomas-wang
*/
static uint64_t __wang_hash(uint64_t key) 
{
	key = (~key) + (key << 21); // key = (key << 21) - key - 1;
	key = key ^ (key >> 24);
	key = (key + (key << 3)) + (key << 8); // key * 265
	key = key ^ (key >> 14);
	key = (key + (key << 2)) + (key << 4); // key * 21
	key = key ^ (key >> 28);
	key = key + (key << 31);
	return key;
}

//!Disassembles the hash linked list
static void h_llist_disassemble(struct h_llist *list) 
{
	while (list) {
		struct h_llist *temp = list->next;
		list = temp;
	}
}

//!Destroys the hash linked list
static void h_llist_destroy(struct h_llist *list) 
{
	while (list) {
		struct h_llist *temp = list->next;
		struct visited_node *vnode = list->h_data;
		if (vnode->priority) {
			free(((struct visited_node *)list->h_data)->priority);
		}
		free(list->h_data);
		free(list);
		list = temp;
	}
}

//!Create a new linked list for the hash table
static struct h_llist *h_llist_create(size_t dev_id, void *data) 
{
	struct h_llist *node = malloc(sizeof(*node));
	if (!node) {
		return NULL;
	}

	node->dev_id = dev_id;
	node->h_data = data;
	node->next = NULL;

	return node;
}

//!Returns an index value from the device ID
static size_t hashish(size_t dev_id, size_t capacity)
{
	return __wang_hash((uint64_t)dev_id) % capacity;
}

//!Walks the index of the hash and prints each list
void hash_print(hash *h)
{
	if (!h) {
		return;
	}
	printf("Hash Print:\n");
	printf(" item_count: %lu\n", h->item_count);
	for (size_t index = 0; index < h->capacity; index++) {
		struct h_llist *cur = h->list[index];
		while (cur) {
			printf(" h_llist device: %9lu\n", cur->dev_id);
			cur = cur->next;
		}
	}
}

