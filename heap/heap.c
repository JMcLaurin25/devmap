
#include <stdio.h>
#include "heap.h"

/*
For a thorough documentation see /docs/index.html
*/

/*!\file
	Heap functions were built off of foundational exercises
	in datastructure lessons. 
	https://github.com/umbctraining/datastructures/tree/master/heap/
 */

static size_t DEFAULT_HEAP_CAPACITY = 16;

#define PARENT(n) (((n)-1)/2)
#define LEFT(n) (2*(n) + 1)
#define RIGHT(n) (2*(n) + 2)

static bool __resize_heap(heap *h);
static void __heapify(heap *h, size_t n);

//!Create new heap
heap *heap_create(int (*cmp)(void *, void *))
{
	if(!cmp) {
		return NULL;
	}

	heap *h = malloc(sizeof(*h));
	if(!h) {
		return NULL;
	}

	h->data = malloc(DEFAULT_HEAP_CAPACITY * sizeof(*h->data));
	if(!h->data) {
		free(h);
		return NULL;
	}

	h->size = 0;
	h->capacity = DEFAULT_HEAP_CAPACITY;
	h->cmp = cmp;

	return h;
}

//!Disassemble the heap links, maintaining the data
void heap_disassemble(heap *h)
{
	if(!h) {
		return;
	}

	free(h->data);
	free(h);
}

//!Destroy the head and its data
void heap_destroy(heap *h)
{
	if(!h) {
		return;
	}

	for(size_t n = 0; n < h->size; ++n) {
		free(h->data[n]);
	}

	free(h->data);
	free(h);
}

//!Return (True/False) of heaps occupancy
bool heap_is_empty(heap *h)
{
	return !(h && h->size > 0);
}

//!Return size of the heap
size_t heap_size(heap *h)
{
	if(!h) {
		return 0;
	}

	return h->size;
}

//!Add new element to heap. Returns true if successful
bool heap_add(heap *h, void *value)
{
	if(!h) {
		return false;
	}

	if(h->size == h->capacity) {
		if(!__resize_heap(h)) {
			return false;
		}
	}

	size_t n = h->size;
	h->data[n] = value;

	while(n > 0 && h->cmp(value, h->data[PARENT(n)]) < 0) {
		h->data[n] = h->data[PARENT(n)];
		n = PARENT(n);
		h->data[n] = value;
	}

	++h->size;

	return true;
}

//!Returns top of heap
void *heap_peek_min(heap *h)
{
	if(!h || h->size == 0) {
		return NULL;
	}

	return h->data[0];
}

//!Return the first value from the heap (index = 0)
void *heap_remove_min(heap *h)
{
	if(!h || h->size == 0) {
		return NULL;
	}

	void *value = h->data[0];

	h->data[0] = h->data[--h->size];

	__heapify(h, 0);

	return value;
}

void heap_rebalance(heap *h)
{
	if(!h) {
		return;
	}

	size_t pos = (h->size-1)/2;
	while(pos) {
		__heapify(h, pos);
		--pos;
	}
}


static bool __resize_heap(heap *h)
{
	void **temp = realloc(h->data, (2 * h->capacity * sizeof(*h->data)));
	if(temp) {
		h->data = temp;
		h->capacity *= 2;
		return true;
	}

	return false;
}

static void __heapify(heap *h, size_t n)
{
	while(LEFT(n) < h->size) {
		size_t best = n;
		size_t left = LEFT(n);
		size_t right = RIGHT(n);

		if(h->cmp(h->data[left], h->data[best]) < 0) {
			best = left;
		}
		if(right < h->size && h->cmp(h->data[right], h->data[best]) < 0) {
			best = right;
		}

		if(best == n) {
			break;
		}

		void *tmp = h->data[best];
		h->data[best] = h->data[n];
		h->data[n] = tmp;
		n = best;
	}
}
