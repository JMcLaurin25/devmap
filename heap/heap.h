
#ifndef HEAP_H
 #define HEAP_H

// Min heap (max heap just requires the inverse of the cmp function)

#include <stdbool.h>
#include <stdlib.h>

struct _heap {
	size_t size;
	size_t capacity;
	int (*cmp)(void *, void *);
	void **data;
};

typedef struct _heap heap;

heap *heap_create(int (*cmp)(void *, void *));
void heap_disassemble(heap *h);
void heap_destroy(heap *h);

bool heap_is_empty(heap *h);
size_t heap_size(heap *h);

void heap_rebalance(heap *h);

bool heap_add(heap *h, void *value);
void *heap_peek_min(heap *h);
void *heap_remove_min(heap *h);

#endif
